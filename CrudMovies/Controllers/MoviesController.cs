﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CrudMovies.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace CrudMovies.Controllers
{
    public class MoviesController : Controller
    {
        private readonly ApplicationDbContext _db;

        public MoviesController(ApplicationDbContext db)
        {
            _db = db;
        }

        public IActionResult Index()
        {
            var displaydata = _db.Movies.ToList();
            return View(displaydata);
        }

        [HttpGet]
        public async Task<IActionResult> Index(string movSearch)
        {
            ViewData["GetMoviesDetails"] = movSearch;
            var empquery = from x in _db.Movies select x;
            if (!String.IsNullOrEmpty(movSearch))
            {
                empquery = empquery.Where(x => x.nombreMov.Contains(movSearch) ||
                x.generoMov.Contains(movSearch));
            }
            return View(await empquery.AsNoTracking().ToListAsync());
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Create(Movies nMov)
        {
            if (ModelState.IsValid)
            {
                _db.Add(nMov);
                await _db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(nMov);
        }

        public async Task<IActionResult> Detail(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }
            var getMovDetail = await _db.Movies.FindAsync(id);
            return View(getMovDetail);
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }
            var getMovDetail = await _db.Movies.FindAsync(id);
            return View(getMovDetail);
        }


        [HttpPost]
        public async Task<ActionResult> Edit(Movies oldMov)
        {
            if (ModelState.IsValid)
            {
                _db.Update(oldMov);
                await _db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(oldMov);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }
            var getMovDetail = await _db.Movies.FindAsync(id);
            return View(getMovDetail);
        }

        [HttpPost]
        public async Task<ActionResult> Delete(int id)
        {

            var getMovDetail = await _db.Movies.FindAsync(id);
            _db.Movies.Remove(getMovDetail);
            await _db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

    }
}
