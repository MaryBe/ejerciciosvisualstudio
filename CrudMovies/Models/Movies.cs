﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CrudMovies.Models
{
    public class Movies
    {
        [Key]
        public int idMov { get; set; }

        [Required (ErrorMessage = "Ingrese titulo de la pelicula")]
        [Display(Name = "Titulo")]
        public string nombreMov { get; set; }

        [Required (ErrorMessage = "Ingrese nombre del director")]
        [Display(Name = "Director")]
        public string directorMov { get; set; }

        [Required (ErrorMessage ="Ingrese duracion en minutos")]
        [Display(Name = "Duracion en minutos")]
        public int duracionMov { get; set; }
        

        [Required (ErrorMessage ="Ingrese año de estreno")]
        [Display(Name = "Estreno")]
        public int fechaMov { get; set; }


        [Required(ErrorMessage = "Seleccione genero")]
        [Display(Name = "Genero")]
        public string generoMov { get; set; }
    }
}
