﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace CrudClientes.Models
{
    public class Cliente
    {
        [Key]
        public int IdCliente { get; set; }

        [Required(ErrorMessage = "Ingresa nombre del cliente")]
        [Display(Name = "Nombre")]
        public string NombreCliente { get; set; }

        [Required(ErrorMessage = "Ingresa credito del cliente")]
        [Display(Name = "Credito")]
        public int CreditoCliente { get; set; }

        [Required(ErrorMessage = "Ingresa edad del cliente")]
        [Display(Name = "Edad")]
        [Range(20, 50)]
        public int EdadCliente { get; set; }

        [Required(ErrorMessage = "Ingresa tu correo")]
        [Display(Name = "Email")]
        [EmailAddress(ErrorMessage = "Ingresa un correo valido")]
        public string CorreoCliente { get; set; }



    }
}
