create database MOV;

use MOV;

drop table movies;

Create table movies
(
idMov INT not null primary key identity (1,1),
nombreMov nVARCHAR (150),
directorMov nvarchar (150),
duracionMov int,
fechaMov int,
generoMov nVarchar(15)
);



insert into movies (nombreMov, directorMov, duracionMov, fechaMov, generoMov)
values ('Titanic', 'James Cameron', 195, 1997,'romance');

select * from movies;





Create table movies
(
idMov INT not null primary key identity (1,1),
tituloMov nVARCHAR (150),
generoMov nVarchar(15),
fechaMov Date,
horario nVarchar (8),
reservoMov nvarchar (150),
asientos int,
tipoPago nVarchar (20)
);

insert into movies (tituloMov, generoMov, fechaMov, horario, reservoMov, asientos,tipoPago)
values ('Titanic', 'Romance', 2020/07/12, '1:00pm','Maricruz Bernal',3, Efectivo);

select * from movies;