﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace REST_CLIENTE.Models
{
    [MetadataType(typeof(cliente.MetaData))]
    public partial class cliente
    {
        sealed class MetaData
        {
            [Key]
            public int IdCliente;

            [Required(ErrorMessage ="Ingresa el nombre")]
            public string NombreCliente;

            [Required]
            [Range(0,2000,ErrorMessage ="Ingresa credito")]
            public Nullable<int> CreditoCliente;

            [Required]
            [Range(18,50,ErrorMessage ="Ingresa edad")]
            public Nullable<int> EdadCliente;

            [Required]
            [EmailAddress(ErrorMessage ="Ingresa correo valido")]
            public string CorreoCliente;
        }
    }
}