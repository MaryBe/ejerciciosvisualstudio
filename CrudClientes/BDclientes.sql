DROP DATABASE CLI;

CREATE DATABASE CLI;

USE CLI;

DROP TABLE cliente

CREATE TABLE cliente
(
IdCliente int not null primary key identity (1,1),
NombreCliente nvarchar (50),
CreditoCliente int,
EdadCliente int,
CorreoCliente nvarchar (150)
)

insert into cliente (NombreCliente, CreditoCliente, EdadCliente, CorreoCliente) 
values ('Maricruz', 2000, 23, 'mari.bernal@outlook.com');

select * from cliente;