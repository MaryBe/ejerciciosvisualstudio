﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CrudClientes.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace CrudClientes.Controllers
{
    public class ClienteController : Controller
    {
        private readonly ApplicationDbContext _db;

        public ClienteController(ApplicationDbContext db)
        {
            _db = db;
        }

        public IActionResult Index()
        {
            var displaydata = _db.Cliente.ToList();
            return View(displaydata);
        }

        [HttpGet]
        public async Task<IActionResult> Index(string cliSearch)
        {
            ViewData["GetCliDetails"] = cliSearch;
            var empquery = from x in _db.Cliente select x;
            if (!String.IsNullOrEmpty(cliSearch))
            {
                empquery = empquery.Where(x => x.NombreCliente.Contains(cliSearch) ||
                x.CorreoCliente.Contains(cliSearch));
            }
            return View(await empquery.AsNoTracking().ToListAsync());
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Create(Cliente nCli)
        {
            if (ModelState.IsValid)
            {
                _db.Add(nCli);
                await _db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(nCli);
        }

        public async Task<IActionResult> Detail(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }
            var getCliDetail = await _db.Cliente.FindAsync(id);
            return View(getCliDetail);
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }
            var getCliDetail = await _db.Cliente.FindAsync(id);
            return View(getCliDetail);
        }

        [HttpPost]
        public async Task<ActionResult> Edit(Cliente oldCli)
        {
            if (ModelState.IsValid)
            {
                _db.Update(oldCli);
                await _db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(oldCli);
        }


        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }
            var getCliDetail = await _db.Cliente.FindAsync(id);
            return View(getCliDetail);
        }

        [HttpPost]
        public async Task<ActionResult> Delete(int id)
        {

            var getCliDetail = await _db.Cliente.FindAsync(id);
            _db.Cliente.Remove(getCliDetail);
            await _db.SaveChangesAsync();
            return RedirectToAction("Index");
        }




    }
}
